PRODUCT_SOONG_NAMESPACES += \
    vendor/addons/oneplus

PRODUCT_COPY_FILES += \
    vendor/addons/oneplus/proprietary/product/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/addons/oneplus/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/addons/oneplus/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml \
    vendor/addons/oneplus/proprietary/system/framework/oplus-framework.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/oplus-framework.jar \
    vendor/addons/oneplus/proprietary/system/framework/oplus-services.jar:$(TARGET_COPY_OUT_SYSTEM)/framework/oplus-services.jar \
    vendor/addons/oneplus/proprietary/system_ext/etc/permissions/com.coloros.gallery3d.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.coloros.gallery3d.xml \
    vendor/addons/oneplus/proprietary/system_ext/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.camera.service.xml

PRODUCT_PACKAGES += \
    OplusCamera \
    OppoGallery2
